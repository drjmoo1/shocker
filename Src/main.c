/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BUFFSIZE 1024
#define ON 1
#define OFF 0

#define POS 1
#define NEG 0

// test results
#define NORESULT 3
#define ERROR 2
#define PASS 1
#define FAIL 0


// status
#define INIT 0
#define TEST 1
#define SHOCK 2
#define IDLE 3
#define DEBUG 4
#define ABORT 5

// pass threshold
#define PASSTHRESHOLD 375

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
static void runCMD(void);

static void channel1CMD(uint8_t state);
static void channel2CMD(uint8_t state);
static void channel3CMD(uint8_t state);
static void channel4CMD(uint8_t state);
static void portCheckCMD(uint8_t state);
static void ioGoodCMD(uint8_t state);
static void ioBadCMD(uint8_t state);
static void zvsCMD(uint8_t state);

static void getCapVoltage(void);

static void abortSEQ(void);
static void testSEQ(uint8_t channel);
static void shockSEQ(uint8_t channel, uint8_t pulse, uint8_t number);


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t newCMD = 0;
uint8_t currentCMD[BUFFSIZE];
uint8_t currentPort = 111;
uint8_t status = INIT;

uint8_t rx_data;
uint8_t rx_index = 0;
uint8_t rx_buff[BUFFSIZE];

uint32_t capVoltage = 0;


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Transmit(&huart1, (uint8_t *)"\n\rShocker V 1.0\n\r", sizeof("\n\rShocker V 0.9\n\r"), 100);
  zvsCMD(ON);
  status = IDLE;


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  getCapVoltage();

	  if (newCMD == 1)
	  {
		  runCMD();
		  currentPort = 111;
	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL8;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = ENABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_9;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 3;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 159;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OnePulse_Init(&htim1, TIM_OPMODE_SINGLE) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_DISABLE;
  sSlaveConfig.InputTrigger = TIM_TS_ITR0;
  if (HAL_TIM_SlaveConfigSynchronization(&htim1, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM2;
  sConfigOC.Pulse = 79;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM2;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */
  HAL_UART_Receive_IT (&huart1, &rx_data, 1);
  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, channel1_Pin|channel2_Pin|channel3_Pin|channel4_Pin 
                          |On_off_ZVS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CHECK_ON_GPIO_Port, CHECK_ON_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, IO_good_Pin|IO_bad_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : channel1_Pin channel2_Pin channel3_Pin channel4_Pin 
                           On_off_ZVS_Pin */
  GPIO_InitStruct.Pin = channel1_Pin|channel2_Pin|channel3_Pin|channel4_Pin 
                          |On_off_ZVS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : CHECK_ON_Pin */
  GPIO_InitStruct.Pin = CHECK_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CHECK_ON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IO_good_Pin IO_bad_Pin */
  GPIO_InitStruct.Pin = IO_good_Pin|IO_bad_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
static void shockSEQ(uint8_t channel, uint8_t pulse, uint8_t number)
{
	TIM_OC_InitTypeDef sConfigOC = {0};
	status = SHOCK;

	if(channel==1)
	{
		channel1CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"shocking port 1\n\r", sizeof("shocking port 1\n\r"), 100);
	}
	else if(channel==2)
	{
		channel2CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"shocking port 2\n\r", sizeof("shocking port 2\n\r"), 100);
	}
	else if(channel==3)
	{
		channel3CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"shocking port 3\n\r", sizeof("shocking port 3\n\r"), 100);
	}
	else if(channel==4)
	{
		channel4CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"shocking port 4\n\r", sizeof("shocking port 4\n\r"), 100);
	}

	if(pulse==POS)
	{
		  sConfigOC.OCMode = TIM_OCMODE_PWM2;
		  sConfigOC.Pulse = 79;
		  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
		  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
		  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
		  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
		  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;

		  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  sConfigOC.OCMode = TIM_OCMODE_PWM1;

		  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  sConfigOC.OCMode = TIM_OCMODE_FORCED_ACTIVE;
		  sConfigOC.Pulse = 0;
		  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
		  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  sConfigOC.OCMode = TIM_OCMODE_FORCED_INACTIVE;
		  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
		  {
		    Error_Handler();
		  }

		ioBadCMD(ON);

		for(uint8_t i=0; i<number; i++)
		{
			if(status==ABORT)
			{
				break;
			}
			HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_4);
			HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);
			HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);

			HAL_Delay(1200);

			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);
			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_4);
			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);

			HAL_GPIO_TogglePin(GPIOB, IO_bad_Pin);
			HAL_UART_Transmit(&huart1, (uint8_t *)".", sizeof("."), 100);
		}
	}
	else if(pulse==NEG)
	{
		  sConfigOC.OCMode = TIM_OCMODE_PWM2;
		  sConfigOC.Pulse = 79;
		  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
		  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
		  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
		  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
		  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;

		  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  sConfigOC.OCMode = TIM_OCMODE_PWM1;

		  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  sConfigOC.OCMode = TIM_OCMODE_FORCED_ACTIVE;
		  sConfigOC.Pulse = 0;
		  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
		  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  sConfigOC.OCMode = TIM_OCMODE_FORCED_INACTIVE;
		  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
		  {
		    Error_Handler();
		  }

		ioBadCMD(ON);

		for(uint8_t i=0; i<number; i++)
		{
			if(status==ABORT)
			{
				break;
			}
			HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);
			HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_4);
			HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3);

			HAL_Delay(1200);

			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_4);
			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
			HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);

			HAL_GPIO_TogglePin(GPIOB, IO_bad_Pin);
			HAL_UART_Transmit(&huart1, (uint8_t *)".", sizeof("."), 100);
		}
	}
	if(status!=ABORT)
	{
		HAL_UART_Transmit(&huart1, (uint8_t *)"\n\rSHOCK COMPLETE\n\r", sizeof("\n\rSHOCK COMPLETE\n\r"), 100);

		ioBadCMD(OFF);

		channel1CMD(OFF);
		channel2CMD(OFF);
		channel3CMD(OFF);
		channel4CMD(OFF);
	}
	status=IDLE;
}

static void testSEQ(uint8_t channel)
{
	status = TEST;
	uint8_t result = NORESULT;

	volatile uint32_t checkVoltage1 = 0;
	volatile uint32_t checkVoltage2 = 0;

	volatile uint32_t checkAvg = 0;

	if(channel==1)
	{
		channel1CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"testing port 1\n\r", sizeof("testing port 1\n\r"), 100);
	}
	else if(channel==2)
	{
		channel2CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"testing port 2\n\r", sizeof("testing port 2\n\r"), 100);
	}
	else if(channel==3)
	{
		channel3CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"testing port 3\n\r", sizeof("testing port 3\n\r"), 100);
	}
	else if(channel==4)
	{
		channel4CMD(ON);
		HAL_UART_Transmit(&huart1, (uint8_t *)"testing port 4\n\r", sizeof("testing port 4\n\r"), 100);
	}
	else
	{
		result = ERROR;
		__NOP();
	}

	portCheckCMD(ON);

	HAL_ADC_Start(&hadc);
	HAL_Delay(500);

	HAL_ADC_PollForConversion(&hadc, 1000);
	capVoltage = HAL_ADC_GetValue(&hadc);

	HAL_ADC_PollForConversion(&hadc, 1000);
	checkVoltage1 = HAL_ADC_GetValue(&hadc);

	HAL_ADC_PollForConversion(&hadc, 1000);
	capVoltage = HAL_ADC_GetValue(&hadc);

	HAL_ADC_PollForConversion(&hadc, 1000);
	checkVoltage2 = HAL_ADC_GetValue(&hadc);

	HAL_ADC_Stop(&hadc);
	HAL_Delay(250);

	checkAvg = (checkVoltage1+checkVoltage2)/2;

	if(checkAvg < PASSTHRESHOLD)
	{
		result = FAIL;
		HAL_UART_Transmit(&huart1, (uint8_t *)"FAIL\n\r", sizeof("FAIL\n\r"), 100);
		ioBadCMD(ON);
	}

	else if(checkAvg >= PASSTHRESHOLD)
	{
		result = PASS;
		HAL_UART_Transmit(&huart1, (uint8_t *)"PASS\n\r", sizeof("PASS\n\r"), 100);
		ioGoodCMD(ON);
	}

	else
	{
		result = ERROR;
		__NOP();
	}

	if(status!=ABORT)
	{
		portCheckCMD(OFF);

		channel1CMD(OFF);
		channel2CMD(OFF);
		channel3CMD(OFF);
		channel4CMD(OFF);

		HAL_Delay(2000);

		ioGoodCMD(OFF);
		ioBadCMD(OFF);
	}
	status = IDLE;
}

static void getCapVoltage(void)
{
	HAL_ADC_Start(&hadc);

	HAL_Delay(500);

	HAL_ADC_PollForConversion(&hadc, 1000);
	capVoltage = HAL_ADC_GetValue(&hadc);
	HAL_ADC_Stop(&hadc);

	HAL_Delay(25);
}

static void abortSEQ(void)
{
	if(status==SHOCK)
	{
		status = ABORT;
	}
	else if(status==TEST)
	{
		status = ABORT;
		portCheckCMD(OFF);
	}

	channel1CMD(OFF);
	channel2CMD(OFF);
	channel3CMD(OFF);
	channel4CMD(OFF);

	ioGoodCMD(OFF);
	ioBadCMD(OFF);
}

static void ioBadCMD(uint8_t state)
{
	if(state==OFF)
	{
		HAL_GPIO_WritePin(GPIOB, IO_bad_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOB, IO_bad_Pin, GPIO_PIN_RESET);
	}
}

static void ioGoodCMD(uint8_t state)
{
	if(state==OFF)
	{
		HAL_GPIO_WritePin(GPIOB, IO_good_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOB, IO_good_Pin, GPIO_PIN_RESET);
	}
}

static void zvsCMD(uint8_t state)
{
	if(state==ON)
	{
		HAL_GPIO_WritePin(GPIOB, CHECK_ON_Pin, GPIO_PIN_SET);
		HAL_UART_Transmit(&huart1, (uint8_t *)"zvs on\n\r", sizeof("zvs on\n\r"), 100);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOB, CHECK_ON_Pin, GPIO_PIN_RESET);
		HAL_UART_Transmit(&huart1, (uint8_t *)"zvs off\n\r", sizeof("zvs off\n\r"), 100);
	}
}

static void portCheckCMD(uint8_t state)
{
	if(state==ON)
	{
		HAL_GPIO_WritePin(GPIOB, CHECK_ON_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOB, CHECK_ON_Pin, GPIO_PIN_RESET);
	}
}

static void channel1CMD(uint8_t state)
{
	if(state==ON)
	{
		HAL_GPIO_WritePin(GPIOA, channel1_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA, channel1_Pin, GPIO_PIN_RESET);
	}
}

static void channel2CMD(uint8_t state)
{
	if(state==ON)
	{
		HAL_GPIO_WritePin(GPIOA, channel2_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA, channel2_Pin, GPIO_PIN_RESET);
	}
}

static void channel3CMD(uint8_t state)
{
	if(state==ON)
	{
		HAL_GPIO_WritePin(GPIOA, channel3_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA, channel3_Pin, GPIO_PIN_RESET);
	}
}

static void channel4CMD(uint8_t state)
{
	if(state==ON)
	{
		HAL_GPIO_WritePin(GPIOA, channel4_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA, channel4_Pin, GPIO_PIN_RESET);
	}
}

void runCMD(void)
{
	newCMD = 0;

	if(strcmp((char *)currentCMD,"test p1") == 0)
	{
		currentPort = 1;
		testSEQ(1);
	}


	else if(strcmp((char *)currentCMD,"test p2") == 0)
	{
		currentPort = 2;
		testSEQ(2);
	}

	else if(strcmp((char *)currentCMD,"test p3") == 0)
	{
		currentPort = 3;
		testSEQ(3);
	}

	else if(strcmp((char *)currentCMD,"test p4") == 0)
	{
		currentPort = 4;
		testSEQ(4);
	}

	else if(strcmp((char *)currentCMD,"shock p1 neg") == 0)
	{
		currentPort = 1;
		shockSEQ(1, NEG, 50);
	}

	else if(strcmp((char *)currentCMD,"shock p1 pos") == 0)
	{
		currentPort = 1;
		shockSEQ(1, POS, 50);
	}

	else if(strcmp((char *)currentCMD,"shock p2 neg") == 0)
	{
		currentPort = 2;
		shockSEQ(2, NEG, 50);
	}

	else if(strcmp((char *)currentCMD,"shock p2 pos") == 0)
	{
		currentPort = 2;
		shockSEQ(2, POS, 50);
	}

	else if(strcmp((char *)currentCMD,"shock p3 neg") == 0)
	{
		currentPort = 3;
		shockSEQ(3, NEG, 50);
	}

	else if(strcmp((char *)currentCMD,"shock p3 pos") == 0)
	{
		currentPort = 3;
		shockSEQ(3, POS, 50);
	}

	else if(strcmp((char *)currentCMD,"shock p4 neg") == 0)
	{
		currentPort = 4;
		shockSEQ(4, NEG, 50);
	}

	else if(strcmp((char *)currentCMD,"shock p4 pos") == 0)
	{
		currentPort = 4;
		shockSEQ(4, POS, 50);
	}
	else if(strcmp((char *)currentCMD,"zvs on") == 0)
	{
		zvsCMD(ON);
	}

	else if(strcmp((char *)currentCMD,"zvs off") == 0)
	{
		zvsCMD(OFF);
	}
	else if(strcmp((char *)currentCMD,"red led on") == 0)
	{
		ioBadCMD(ON);
	}

	else if(strcmp((char *)currentCMD,"red led off") == 0)
	{
		ioBadCMD(OFF);
	}
	else if(strcmp((char *)currentCMD,"green led on") == 0)
	{
		ioGoodCMD(ON);
	}

	else if(strcmp((char *)currentCMD,"green led off") == 0)
	{
		ioGoodCMD(OFF);
	}
	else if(strcmp((char *)currentCMD,"cap?") == 0)
	{
		char printbuff[6];
		sprintf(printbuff, "%d", capVoltage);
		HAL_UART_Transmit(&huart1, (uint8_t *)&printbuff, sizeof(printbuff), 100);
		HAL_UART_Transmit(&huart1, (uint8_t *)"\n\r", sizeof("\n\r"), 100);
	}
	else if(strcmp((char *)currentCMD,"status?") == 0)
	{
		if(status==INIT)
		{
			HAL_UART_Transmit(&huart1, (uint8_t *)"Status:INIT\n\r", sizeof("Status:INIT\n\r"), 100);
		}
		else if(status==TEST)
		{
			HAL_UART_Transmit(&huart1, (uint8_t *)"Status:TEST\n\r", sizeof("Status:TEST\n\r"), 100);
		}
		else if(status==SHOCK)
		{
			HAL_UART_Transmit(&huart1, (uint8_t *)"Status:SHOCK\n\r", sizeof("Status:SHOCK\n\r"), 100);
		}
		else if(status==IDLE)
		{
			HAL_UART_Transmit(&huart1, (uint8_t *)"Status:IDLE\n\r", sizeof("Status:IDLE\n\r"), 100);
		}
		else if(status==DEBUG)
		{
			HAL_UART_Transmit(&huart1, (uint8_t *)"Status:DEBUG\n\r", sizeof("Status:DEBUG\n\r"), 100);
		}
		else if(status==ABORT)
		{
			HAL_UART_Transmit(&huart1, (uint8_t *)"Status:ABORT\n\r", sizeof("Status:ABORT\n\r"), 100);
		}
	}
	else if(strcmp((char *)currentCMD,"abort") == 0)
	{
		HAL_UART_Transmit(&huart1, (uint8_t *)"ABORTED!\n\r", sizeof("ABORTED!\n\r"), 100);
	}
	else
	{
		HAL_UART_Transmit(&huart1, (uint8_t *)"CMD NOT FOUND!\n\r", sizeof("CMD NOT FOUND!\n\r"), 100);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART1)
	{
		if (rx_data == 13)
		{
			strcpy((char *)currentCMD, (char *)rx_buff);
			newCMD = 1;
			if(strcmp((char *)currentCMD,"abort") == 0)
			{
				abortSEQ();
			}
			HAL_UART_Transmit(&huart1, (uint8_t *)"\n\r", sizeof("\n\r"), 100);
			for (int i=0; i<=rx_index; i++)
			{
				rx_buff[i] = 0;
			}
			rx_index = 0;
		}
		else
		{
			rx_buff[rx_index++] = rx_data;
			HAL_UART_Transmit(&huart1, (uint8_t *)&rx_data, sizeof(rx_data), 100);
		}

		HAL_UART_Receive_IT(&huart1, &rx_data, 1);
	}
}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
